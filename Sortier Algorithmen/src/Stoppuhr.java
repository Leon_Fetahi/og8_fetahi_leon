
public class Stoppuhr {
	long startzeit;
	long endzeit;
	
	public void start() {
		startzeit = System.currentTimeMillis();
	}
	
	public void stop() {
		endzeit = System.currentTimeMillis();
	}
	
	public void reset() {
		startzeit = 0;
		endzeit = 0;
	}
	
	public long getDauer() {
		return endzeit - startzeit;
	}
}
