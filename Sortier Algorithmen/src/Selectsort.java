
public class Selectsort {
	int temp;
	int platz;
	
	public int[] sortieren(int[] a){
		System.out.println("Start: " + java.util.Arrays.toString(a));
		for(int n = a.length; n>0; n--) {
		platz = 0;
		for(int i = 0; i < n ;i++)
			if(a[platz]<a[i]) {
				platz = i;
			}
		temp = a[n-1];
		a[n-1] = a[platz];
		a[platz] = temp;
		System.out.println("{" + (n-1) + " -> " + platz + "} " + java.util.Arrays.toString(a));
		}
		return a;
}
}
