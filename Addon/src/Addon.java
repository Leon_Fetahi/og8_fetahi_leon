
public class Addon {
	
private int idNummer;
private String bezeichnung;
private double preis;
private int bestand;
private int bestandMax;

Addon(){
	
}

public int getIDNummer() {
	return idNummer;
}

public String getBezeichnung() {
	return bezeichnung;
}

public double getPreis() {
	return preis;
}

public int getBestand() {
	return bestand;
}

public int getBestandMax() {
	return bestandMax;
}

public void setIDNummer(int newIDNummer) {
	idNummer = newIDNummer;
}

public void setBezeichnung(String newBezeichnung) {
	bezeichnung = newBezeichnung;
}

public void setPreis(double newPreis) {
	preis = newPreis;
}

public void setBestand(int newBestand) {
	bestand = newBestand;
}

public void setBestandMax(int newBestandMax) {
	bestandMax = newBestandMax;
}

public void aendereBestand(int wert) {
	bestand += wert;
}

public double berechneGesammtWert() {
	return bestand * preis;
}
}
