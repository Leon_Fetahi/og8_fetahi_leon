import java.util.TreeSet;

public class KeyStore03 {
	TreeSet<String> tree = new TreeSet();

	public boolean add(String eintrag) {
		tree.add(eintrag);
		return true;
	}

	public int indexOf(String eintrag) {
		for(int i = 0;i<tree.toArray().length;i++) {
			if(tree.toArray()[i].equals(eintrag)) return i;
		}
		return -1;
	}

	public boolean remove(int index) {
		tree.remove(tree.toArray()[index]);
		return true;
	}

	public String toString() {
		return "KeyStore03: " + tree.toString() ;
	}

	
}
