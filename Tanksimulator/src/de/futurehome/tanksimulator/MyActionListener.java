package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

public class MyActionListener implements ActionListener {
	public TankSimulator f;
	
	
	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 fuellstand = fuellstand + 5;
			 if(fuellstand>100)
				 fuellstand = 100;
			 
			 f.myTank.setFuellstand(fuellstand);
			 
			 f.jpbFuellstand.setValue((int)fuellstand);
			 f.lblFuellstand.setText(fuellstand+"L              "+fuellstand+"%");
			 System.out.println(LocalTime.now()+" | Der Tank wurde um 5 aufgef�hlt und ist auf einem Wert von " + fuellstand);
		}

		if (obj == f.btnVerbrauchen) {
			 double fuellstand = f.myTank.getFuellstand();
			 if(fuellstand>f.jsVerbrauch.getValue())
			 fuellstand = fuellstand - f.jsVerbrauch.getValue();
			 else
				 fuellstand = 0;
			 
			 f.myTank.setFuellstand(fuellstand);
			 
			 f.jpbFuellstand.setValue((int)fuellstand);
			 f.lblFuellstand.setText(fuellstand+"L               "+fuellstand+"%");
			 System.out.println(LocalTime.now()+" | Der Tank wurde um "+ f.jsVerbrauch.getValue() +" aufgef�hlt und ist auf einem Wert von " + fuellstand);
		}
		
		if (obj == f.btnZuruecksetzen) {
			 f.myTank.setFuellstand(0);
			 
			 f.jpbFuellstand.setValue(0);
			 f.lblFuellstand.setText("0.0L"+"               "+"0.0%");
			 System.out.println(LocalTime.now()+" | Der Tank wurde auf 0 Zur�ckgesetzt");
		}
	}
}