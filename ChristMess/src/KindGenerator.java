import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class KindGenerator{

	//Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den Daten eine Liste an Kind-Objekte anlegen
	
	ArrayList<Kind> liste = new ArrayList<Kind>();
	
	private File file;
	
	KindGenerator(File file){
		this.file = file;
	}
	
	public ArrayList<Kind> auslesen() {
		try{
		FileReader fr = new FileReader(this.file);
		BufferedReader br = new BufferedReader(fr);
		String s;
		while((s = br.readLine()) != null) {
			String[] parts =  s.split(", ");
			String[] name = parts[0].split(" ");
			liste.add(new Kind(name[0],name[1],parts[1],parts[3],Integer.parseInt(parts[2])));
		}
		
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("Ein Fehler ist beim auslesen der Datei entstanden");	
	}
		return liste;
}
	public int compare(Kind a, Kind b) {
		return a.getBravheitsgrad() - b.getBravheitsgrad();
	}
}