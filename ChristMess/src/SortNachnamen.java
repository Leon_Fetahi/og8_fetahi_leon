import java.util.Comparator;

public class SortNachnamen implements Comparator<Kind>{
	public int compare(Kind a, Kind b) {
		return a.getNachname().compareTo(b.getNachname());
	}
}
