import java.util.Comparator;

public class SortBravheit implements Comparator<Kind>{
	public int compare(Kind a, Kind b) {
		return a.getBravheitsgrad() - b.getBravheitsgrad();
	}
}
