import java.util.*;

public class Kind{
	public String Nachname, Vorname, Geburtstag, wohnort;
	public int bravheitsgrad;
	
	
	
	public Kind(String nachname, String vorname, String geburtstag, String wohnort, int bravheitsgrad) {
		super();
		Nachname = nachname;
		Vorname = vorname;
		Geburtstag = geburtstag;
		this.wohnort = wohnort;
		this.bravheitsgrad = bravheitsgrad;
	}
	
	public Kind(String nachname, String vorname, String geburtstag, String wohnort) {
		super();
		Nachname = nachname;
		Vorname = vorname;
		Geburtstag = geburtstag;
		this.wohnort = wohnort;
	}
	
	public String getNachname() {
		return Nachname;
	}
	public void setNachname(String nachname) {
		Nachname = nachname;
	}
	public String getVorname() {
		return Vorname;
	}
	public void setVorname(String vorname) {
		Vorname = vorname;
	}
	public String getGeburtstag() {
		return Geburtstag;
	}
	public void setGeburtstag(String geburtstag) {
		Geburtstag = geburtstag;
	}
	public String getWohnort() {
		return wohnort;
	}
	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}
	public int getBravheitsgrad() {
		return bravheitsgrad;
	}
	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}
	
	public String toString() {
		return "Kind [Nachname=" + Nachname + ", Vorname=" + Vorname + ", Geburtstag=" + Geburtstag + ", wohnort="
				+ wohnort + ", bravheitsgrad=" + bravheitsgrad + "]";
	}
}
