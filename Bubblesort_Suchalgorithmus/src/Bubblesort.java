public class Bubblesort {
	public int temp;
	public int vergleiche;
	
	public int[] sortiere(int[] a) {
		for(int n = a.length - 1; n>0; n--) {
			for(int i = 0;i<n;i++) {
				if(a[i]>a[i+1]) {
					temp = a[i+1];
					a[i+1] = a[i];
					a[i] = temp;
					vergleiche++;
				}else
					vergleiche++;
			}
		}
		System.out.println("Es wurde " + vergleiche + " oft verglichen.");
		return a;
	}
}