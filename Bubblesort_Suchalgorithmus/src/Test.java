import java.util.Random;

public class Test {
	public static void main(String[] args) {
		int[] a = new int[100000];
		Bubblesort bub = new Bubblesort();
		Stoppuhr stop = new Stoppuhr();
		Random r = new Random();
		
		for(int n = a.length - 1;n>=0;n--)
			a[n] = r.nextInt(a.length - 1);

		stop.start();
		System.out.println(java.util.Arrays.toString(bub.sortiere(a)));
		stop.stop();
		System.out.println("Das Sortieren hat " + stop.getDauer() + " Millisekunden gebraucht");
	}
}