public class Aufgabe4 {
	public int glaeserZaehlen(int n){
		if(n==0) return 0;
		return (n-1)*2 + glaeserZaehlen(n-1);
	}
}
