package taschenrechner;

import java.util.Scanner;

public class Taschenrechnertest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();
		
		int zahl1;
		int zahl2;
		int swValue;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Multiplikation |");
		System.out.println("|        4. Dividieren     |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);

		// Switch construct
		System.out.print(" Select your first number: ");
		zahl1 = myScanner.nextInt();
		System.out.print(" Select your second number: ");
		zahl2 = myScanner.nextInt();
		switch (swValue) {
		case '1':
			System.out.println("4 + 5 = " + ts.add(4, 5));
			break;
			
		case '2':
			System.out.println("4 - 5 = " + ts.sub(4, 5));
			break;
			
		case '3':
			System.out.println("4 * 5 = " + ts.mul(4, 5));
			break;
			
		case '4':
			System.out.println("4 / 5 = " + ts.div(4, 5));
			break;
			
		case '5':
			System.exit(0);
			
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}

	}

}