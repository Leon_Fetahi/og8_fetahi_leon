import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DateiVerwalter {
		private File file;
	
		DateiVerwalter(File file){
			this.file = file;
		}
	
		public void schreiben(String s) {
			try {
				FileWriter fw = new FileWriter(this.file, true);
				BufferedWriter bw = new BufferedWriter(fw);
				
				bw.write(s);
				bw.newLine();
				bw.flush();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Ein Fehler ist beim schreiben einer Datei entstanden");
			}
		}
		
		public void lesen() {
			try {
				FileReader fr = new FileReader(this.file);
				BufferedReader br = new BufferedReader(fr);
				String s;
				while((s = br.readLine()) != null) {
					System.out.println(s);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Ein Fehler ist beim lesen einer Datei entstanden");
			}
		}
}
