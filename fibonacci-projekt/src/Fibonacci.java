/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci{
   // Konstruktor
   Fibonacci(){
   }
   
  /**
    * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
    * @param n 
    * @return die n-te Fibonacci-Zahl
   */
  long fiboRekursiv(int n){
      if(n<=0) return 0;
      if(n<=2) return 1;
      return fiboRekursiv(n-1) + fiboRekursiv(n-2);
  }

  /**
   * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
   * @param n 
   * @return die n-te Fibonacci-Zahl
  */
  long fiboIterativ(int n){
	  long a = 0, b = 1, temp;
	  
	  for (int i = 0; i < n; i++) {
	       temp = a + b;
	       a = b;
	       b = temp;
	  }
	       return a;
  }

}// Fibonnaci


