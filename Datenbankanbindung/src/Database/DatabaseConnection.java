package Database;

import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Connection;

public class DatabaseConnection {
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/keks";
	private String user = "root";
	private String password = "";
	
	public boolean insertKeks(String bezeichnung, String sorte) {
		String sql = "INSERT INTO T_kekse(bezeichnung, sorte) VALUES('"+ bezeichnung +"','"+ sorte +"')";
		
		try {
			//JDBC - Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url,user,password);
			
			Statement stat = con.createStatement();
			stat.executeUpdate(sql);
			
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		DatabaseConnection dbc = new DatabaseConnection();
		dbc.insertKeks("Erdbeerkeks", "sternform");
	}
}
